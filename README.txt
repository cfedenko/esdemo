esdemo README
==================

Getting Started
---------------

- cd <directory containing this file>

- $wget http://downloads.buildout.org/2/bootstrap.py

- $python -S bootstrap.py

- $bin/buildout

- $bin/initialize_esdemo_db development.ini

- $bin/pserve development.ini
