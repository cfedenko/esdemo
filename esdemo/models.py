import urllib

from sqlalchemy import (
    Column,
    Integer,
    String,
    Text,
    ForeignKey,
    )
from sqlalchemy.orm import relation, backref

from pyramid.security import Allow, ALL_PERMISSIONS, Authenticated, Everyone

import pyramid_sqla

Base = pyramid_sqla.get_base()
DBSession = pyramid_sqla.get_session()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    login = Column(String(255), unique=True)
    password = Column(String(255))
    groups = Column(Text)

    @property
    def __acl__(self):
        return [
            (Allow, self.login, 'view'),
        ]

    def __init__(self, login, password, groups=None):
        self.login = login
        self.password = password
        self.groups = " ".join(groups or [])

    def check_password(self, passwd):
        return self.password == passwd

class Page(Base):
    __tablename__ = 'pages'
    id = Column(Integer, primary_key=True)
    title = Column(String(255))
    uri = Column(String(255), unique=True)
    body = Column(Text)
    owner_id = Column(Integer, ForeignKey(User.id))
    owner = relation(User, backref="pages")
    @property
    def __acl__(self):
        return [
            (Allow, self.owner, 'edit'),
            (Allow, 'g:editor', 'edit'),
        ]

    def __init__(self, title, uri, body, owner):
        self.title = title
        self.uri = uri
        self.body = body
        self.owner = owner

def websafe_uri(txt):
    uri = txt.replace(' ', '-')
    return urllib.quote(uri)


class RootFactory(object):
    __acl__ = [
        (Allow, 'g:admin', ALL_PERMISSIONS),
    ]

    def __init__(self, request):
        self.request = request

class UserFactory(object):
    __acl__ = [
        (Allow, 'g:admin', ALL_PERMISSIONS),
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, key):
        user = DBSession.query(User).filter(User.login==key).first()
        user.__parent__ = self
        user.__name__ = key
        return user

class PageFactory(object):
    __acl__ = [
        (Allow, Everyone, 'view'),
        (Allow, Authenticated, 'create'),
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, key):
        page = DBSession.query(Page).filter(Page.uri==key).first()
        page.__parent__ = self
        page.__name__ = key
        return page

def groupfinder(userid, request):
    user = DBSession.query(User).filter(User.login==userid).first()
    if user and user.groups:
        return ['g:%s' % g for g in user.groups.split(" ")]
