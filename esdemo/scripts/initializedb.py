import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from ..models import (
    DBSession,
    User,
    Page,
    Base,
    websafe_uri
    )

def _make_demo_user(login, **kw):
    kw.setdefault('password', login)
    return User(login, **kw)

def _make_demo_page(title, **kw):
    uri = kw.setdefault('uri', websafe_uri(title))
    return Page(title, **kw)


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)

def main(argv=sys.argv):
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
    with transaction.manager:
        luser = _make_demo_user('luser')
        DBSession.add(luser)
        DBSession.add(_make_demo_user('editor', groups=['editor']))
        DBSession.add(_make_demo_user('admin', groups=['admin']))

        DBSession.add(_make_demo_page('hello', owner=luser,
                        body="<h3>Hello World!</h3><p>I'm the body text</p>"))
